#ifndef __FUZZYLOGIC_H__
#define __FUZZYLOGIC_H__

#include <math.h>
#include <set>
#include <stack>
#include <ctime>
#include <string>
#include <iostream>
#include <algorithm>
#include <vector>
#include <deque>
#include <set>


using namespace std;

/////////////////////////////////////////////////////

#define NO_OF_FUZZY_SETS 3
#define INPUTS_PER_FUZZY_SET 2
#define TOTAL_NO_OF_INPUTS 6	//The above two parameters multiplied together
#define NO_OF_INP_REGIONS 5		//AKA Fuzzy Sets, the number of spaces into which a variable is subdivided.
#define NO_OF_OUTPUT_VALUES 9

#define TOO_SMALL 1e-6

//Trapezoidal membership function types
typedef enum { regular_trapezoid, left_trapezoid, right_trapezoid } trapz_type;

//Input parameters
enum { in_theta, in_theta_dot, in_x, in_x_dot, in_combined_theta, in_combined_x };

//Fuzzy sets
enum { in_nl, in_ns, in_ze, in_ps, in_pl };

//Fuzzy output terms
enum { out_nvl, out_nl, out_nm, out_ns, out_ze, out_ps, out_pm, out_pl, out_pvl};

//Three FAMMs, the first two feed into the third
enum { matrix_theta_vs_theta_dot, matrix_x_vs_x_dot, matrix_x_set_vs_theta_set };

typedef struct {
   trapz_type tp;
   float a, b, c, d, l_slope, r_slope;
} trapezoid;

typedef struct {
   short inp_index[INPUTS_PER_FUZZY_SET], inp_fuzzy_set[INPUTS_PER_FUZZY_SET], out_fuzzy_set;
} rule;

typedef struct {
   bool allocated;
   trapezoid inp_mem_fns [TOTAL_NO_OF_INPUTS] [NO_OF_INP_REGIONS];
   rule ** ruleset;
   int no_of_fuzzy_sets, no_of_inputs_per_FAMM, no_of_inp_regions, no_of_rules_per_FAMM, no_of_outputs;
   float output_values[NO_OF_OUTPUT_VALUES];
} fuzzy_system_rec;

extern fuzzy_system_rec g_fuzzy_system;

//---------------------------------------------------------------------------

trapezoid init_trapz (float x1, float x2, float x3, float x4, trapz_type typ);
float fuzzy_system (float inputs[], fuzzy_system_rec fz);
float fuzzy_math (float inputs[], fuzzy_system_rec fz, int FAMM_to_fuzzify);
void free_fuzzy_rules (fuzzy_system_rec *fz);

//-------------------------------------------------------------------------
void initFuzzyRules (fuzzy_system_rec *fl);
void initMembershipFunctions (fuzzy_system_rec *fl); 
void initFuzzySystem (fuzzy_system_rec *fl);

trapezoid init_trapz (float x1, float x2, float x3, float x4, trapz_type typ);
float trapz (float x, trapezoid trz);
float min_of (float values[], int no_of_inps);
float fuzzy_system (float inputOne, float inputTwo, fuzzy_system_rec fz);
void free_fuzzy_rules (fuzzy_system_rec *fz);

#endif
