#include <algorithm>
#include <math.h>
#include "fuzzylogic.h"
#include "transform.h"

/////////////////////////////////////////////////////////////////

//Initialise Fuzzy Rules

void initFuzzyRules(fuzzy_system_rec *fz)
{
	int no_of_rules = pow(NO_OF_INP_REGIONS, INPUTS_PER_FUZZY_SET);
	int i;
	
//---------------------------------------------------------------------------- 	
//THETA vs. THETA_DOT	
//   
	for (i = 0; i < no_of_rules; i++)
	{
		(fz->ruleset[matrix_theta_vs_theta_dot])[i].inp_index[0] = in_theta;
		(fz->ruleset[matrix_theta_vs_theta_dot])[i].inp_index[1] = in_theta_dot;
	}
	
	//Input parameter settings:
	{
		(fz->ruleset[matrix_theta_vs_theta_dot])[0].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[0].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[1].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[1].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_theta_vs_theta_dot])[2].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[2].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_theta_vs_theta_dot])[3].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[3].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_theta_vs_theta_dot])[4].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[4].inp_fuzzy_set[1] = in_pl;
		
		(fz->ruleset[matrix_theta_vs_theta_dot])[5].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[5].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[6].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[6].inp_fuzzy_set[1] = in_ns;
		
		(fz->ruleset[matrix_theta_vs_theta_dot])[7].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[7].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_theta_vs_theta_dot])[8].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[8].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_theta_vs_theta_dot])[9].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[9].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[10].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[10].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[11].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[11].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_theta_vs_theta_dot])[12].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[12].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_theta_vs_theta_dot])[13].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[13].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_theta_vs_theta_dot])[14].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[14].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[15].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[15].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[16].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[16].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_theta_vs_theta_dot])[17].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[17].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_theta_vs_theta_dot])[18].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[18].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_theta_vs_theta_dot])[19].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[19].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[20].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[20].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[21].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[21].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_theta_vs_theta_dot])[22].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[22].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_theta_vs_theta_dot])[23].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[23].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_theta_vs_theta_dot])[24].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[24].inp_fuzzy_set[1] = in_pl;
	}

	//Output parameter settings:
	{
		(fz->ruleset[matrix_theta_vs_theta_dot])[0].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[1].out_fuzzy_set = out_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[2].out_fuzzy_set = out_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[3].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_theta_vs_theta_dot])[4].out_fuzzy_set = out_ns;

		(fz->ruleset[matrix_theta_vs_theta_dot])[5].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[6].out_fuzzy_set = out_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[7].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_theta_vs_theta_dot])[8].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[9].out_fuzzy_set = out_ze;

		(fz->ruleset[matrix_theta_vs_theta_dot])[10].out_fuzzy_set = out_nl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[11].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_theta_vs_theta_dot])[12].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[13].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[14].out_fuzzy_set = out_pm;

		(fz->ruleset[matrix_theta_vs_theta_dot])[15].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_theta_vs_theta_dot])[16].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[17].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_theta_vs_theta_dot])[18].out_fuzzy_set = out_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[19].out_fuzzy_set = out_pvl;

		(fz->ruleset[matrix_theta_vs_theta_dot])[20].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_theta_vs_theta_dot])[21].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_theta_vs_theta_dot])[22].out_fuzzy_set = out_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[23].out_fuzzy_set = out_pl;
		(fz->ruleset[matrix_theta_vs_theta_dot])[24].out_fuzzy_set = out_pvl;
	}

 //----------------------------------------------------------------------------   
 //X vs. X_DOT
 //
	for (i = 0; i < no_of_rules; i++)
	{
		(fz->ruleset[matrix_x_vs_x_dot])[i].inp_index[0] = in_x;
		(fz->ruleset[matrix_x_vs_x_dot])[i].inp_index[1] = in_x_dot;
	}

	//Input parameter settings:
	{
		(fz->ruleset[matrix_x_vs_x_dot])[0].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_vs_x_dot])[1].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_vs_x_dot])[1].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_vs_x_dot])[1].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_vs_x_dot])[2].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_vs_x_dot])[2].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_vs_x_dot])[3].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_vs_x_dot])[3].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_vs_x_dot])[4].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_vs_x_dot])[4].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_vs_x_dot])[5].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[5].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_vs_x_dot])[6].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[6].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_vs_x_dot])[7].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[7].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_vs_x_dot])[8].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[8].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_vs_x_dot])[9].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[9].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_vs_x_dot])[10].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[10].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_vs_x_dot])[11].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[11].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_vs_x_dot])[12].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[12].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_vs_x_dot])[13].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[13].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_vs_x_dot])[14].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[14].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_vs_x_dot])[15].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[15].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_vs_x_dot])[16].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[16].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_vs_x_dot])[17].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[17].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_vs_x_dot])[18].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[18].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_vs_x_dot])[19].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[19].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_vs_x_dot])[20].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_vs_x_dot])[20].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_vs_x_dot])[21].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_vs_x_dot])[21].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_vs_x_dot])[22].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_vs_x_dot])[22].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_vs_x_dot])[23].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_vs_x_dot])[23].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_vs_x_dot])[24].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_vs_x_dot])[24].inp_fuzzy_set[1] = in_pl;
	}

	//Output parameter settings:
	{
		(fz->ruleset[matrix_x_vs_x_dot])[0].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_vs_x_dot])[1].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[2].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[3].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[4].out_fuzzy_set = out_ze;

		(fz->ruleset[matrix_x_vs_x_dot])[5].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_vs_x_dot])[6].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[7].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[8].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[9].out_fuzzy_set = out_ns;

		(fz->ruleset[matrix_x_vs_x_dot])[10].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_vs_x_dot])[11].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[12].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[13].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[14].out_fuzzy_set = out_nm;

		(fz->ruleset[matrix_x_vs_x_dot])[15].out_fuzzy_set = out_ps;
		(fz->ruleset[matrix_x_vs_x_dot])[16].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[17].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[18].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[19].out_fuzzy_set = out_nm;

		(fz->ruleset[matrix_x_vs_x_dot])[20].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_x_vs_x_dot])[21].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[22].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[23].out_fuzzy_set = out_ns;
		(fz->ruleset[matrix_x_vs_x_dot])[24].out_fuzzy_set = out_nm;
	}

//----------------------------------------------------------------------------   
//Combined set of X vs. Combined set of Theta
//
	for (i = 0; i < no_of_rules; i++)
	{
		(fz->ruleset[matrix_x_set_vs_theta_set])[i].inp_index[0] = in_combined_x;
		(fz->ruleset[matrix_x_set_vs_theta_set])[i].inp_index[1] = in_combined_theta;
	}

	//Input parameter settings:
	{
		(fz->ruleset[matrix_x_set_vs_theta_set])[0].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[0].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[1].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[1].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_set_vs_theta_set])[2].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[2].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_set_vs_theta_set])[3].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[3].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_set_vs_theta_set])[4].inp_fuzzy_set[0] = in_nl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[4].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[5].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_set_vs_theta_set])[5].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[6].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_set_vs_theta_set])[6].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_set_vs_theta_set])[7].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_set_vs_theta_set])[7].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_set_vs_theta_set])[8].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_set_vs_theta_set])[8].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_set_vs_theta_set])[9].inp_fuzzy_set[0] = in_ns;
		(fz->ruleset[matrix_x_set_vs_theta_set])[9].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[10].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_set_vs_theta_set])[10].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[11].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_set_vs_theta_set])[11].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_set_vs_theta_set])[12].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_set_vs_theta_set])[12].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_set_vs_theta_set])[13].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_set_vs_theta_set])[13].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_set_vs_theta_set])[14].inp_fuzzy_set[0] = in_ze;
		(fz->ruleset[matrix_x_set_vs_theta_set])[14].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[15].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_set_vs_theta_set])[15].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[16].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_set_vs_theta_set])[16].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_set_vs_theta_set])[17].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_set_vs_theta_set])[17].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_set_vs_theta_set])[18].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_set_vs_theta_set])[18].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_set_vs_theta_set])[19].inp_fuzzy_set[0] = in_ps;
		(fz->ruleset[matrix_x_set_vs_theta_set])[19].inp_fuzzy_set[1] = in_pl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[20].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[20].inp_fuzzy_set[1] = in_nl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[21].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[21].inp_fuzzy_set[1] = in_ns;

		(fz->ruleset[matrix_x_set_vs_theta_set])[22].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[22].inp_fuzzy_set[1] = in_ze;

		(fz->ruleset[matrix_x_set_vs_theta_set])[23].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[23].inp_fuzzy_set[1] = in_ps;

		(fz->ruleset[matrix_x_set_vs_theta_set])[24].inp_fuzzy_set[0] = in_pl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[24].inp_fuzzy_set[1] = in_pl;
	}

	//Output parameter settings:
	{
		(fz->ruleset[matrix_x_set_vs_theta_set])[0].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[1].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[2].out_fuzzy_set = out_pvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[3].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[4].out_fuzzy_set = out_pvl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[5].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[6].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[7].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[8].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[9].out_fuzzy_set = out_pvl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[10].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[11].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[12].out_fuzzy_set = out_ze;
		(fz->ruleset[matrix_x_set_vs_theta_set])[13].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[14].out_fuzzy_set = out_pvl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[15].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[16].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[17].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[18].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[19].out_fuzzy_set = out_pvl;

		(fz->ruleset[matrix_x_set_vs_theta_set])[20].out_fuzzy_set = out_pvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[21].out_fuzzy_set = out_nm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[22].out_fuzzy_set = out_nvl;
		(fz->ruleset[matrix_x_set_vs_theta_set])[23].out_fuzzy_set = out_pm;
		(fz->ruleset[matrix_x_set_vs_theta_set])[24].out_fuzzy_set = out_pvl;
	}
	return;
}

void initMembershipFunctions(fuzzy_system_rec *fz) {

	float tweak = 1;

	/* The theta membership functions */
	fz->inp_mem_fns[in_theta][in_nl] = init_trapz(degToRad(0), degToRad(0), degToRad(-75), degToRad(-60), left_trapezoid);
	fz->inp_mem_fns[in_theta][in_ns] = init_trapz(degToRad(-75), degToRad(-60), degToRad(-30), degToRad(0), regular_trapezoid);
	fz->inp_mem_fns[in_theta][in_ze] = init_trapz(degToRad(-30), degToRad(0), degToRad(0), degToRad(30), regular_trapezoid);
	fz->inp_mem_fns[in_theta][in_ps] = init_trapz(degToRad(0), degToRad(30), degToRad(60), degToRad(75), regular_trapezoid);
	fz->inp_mem_fns[in_theta][in_pl] = init_trapz(degToRad(60), degToRad(75), degToRad(0), degToRad(0), right_trapezoid);

	/* The theta dot membership functions */
	fz->inp_mem_fns[in_theta_dot][in_nl] = init_trapz(degToRad(0), degToRad(0), degToRad(-75), degToRad(-60), left_trapezoid);
	fz->inp_mem_fns[in_theta_dot][in_ns] = init_trapz(degToRad(-75), degToRad(-60), degToRad(-30), degToRad(0), regular_trapezoid);
	fz->inp_mem_fns[in_theta_dot][in_ze] = init_trapz(degToRad(-30), degToRad(0), degToRad(0), degToRad(30), regular_trapezoid);
	fz->inp_mem_fns[in_theta_dot][in_ps] = init_trapz(degToRad(0), degToRad(30), degToRad(60), degToRad(75), regular_trapezoid);
	fz->inp_mem_fns[in_theta_dot][in_pl] = init_trapz(degToRad(60), degToRad(75), degToRad(0), degToRad(0), right_trapezoid);

	/* The X membership functions */
	fz->inp_mem_fns[in_x][in_nl] = init_trapz(0, 0, -2, -1.8, left_trapezoid);
	fz->inp_mem_fns[in_x][in_ns] = init_trapz(-2, -1.6, -0.4, -0, regular_trapezoid);
	fz->inp_mem_fns[in_x][in_ze] = init_trapz(-0.4, -0, 0, 0.4, regular_trapezoid);
	fz->inp_mem_fns[in_x][in_ps] = init_trapz(0, 0.4, 1.6, 2, regular_trapezoid);
	fz->inp_mem_fns[in_x][in_pl] = init_trapz(2, 1.8, 0, 0, right_trapezoid);

	/* The X dot membership functions */
	fz->inp_mem_fns[in_x_dot][in_nl] = init_trapz(0, 0, -3.2, -2.4, left_trapezoid);
	fz->inp_mem_fns[in_x_dot][in_ns] = init_trapz(-3.2, -2.4, -0.8, -0, regular_trapezoid);
	fz->inp_mem_fns[in_x_dot][in_ze] = init_trapz(-0.8, -0, 0, 0.8, regular_trapezoid);
	fz->inp_mem_fns[in_x_dot][in_ps] = init_trapz(0, 0.8, 2.4, 3.2, regular_trapezoid);
	fz->inp_mem_fns[in_x_dot][in_pl] = init_trapz(2.4, 3.2, 0, 0, right_trapezoid);

	/* The combined theta membership functions */
	fz->inp_mem_fns[in_combined_theta][in_nl] = init_trapz(0 * tweak, 0 * tweak, -100 * tweak, -75 * tweak, left_trapezoid);
	fz->inp_mem_fns[in_combined_theta][in_ns] = init_trapz(-100 * tweak, -75 * tweak, -25 * tweak, -0 * tweak, regular_trapezoid);
	fz->inp_mem_fns[in_combined_theta][in_ze] = init_trapz(-25 * tweak, -0 * tweak, 0 * tweak, 25 * tweak, regular_trapezoid);
	fz->inp_mem_fns[in_combined_theta][in_ps] = init_trapz(0 * tweak, 25 * tweak, 75 * tweak, 100 * tweak, regular_trapezoid);
	fz->inp_mem_fns[in_combined_theta][in_pl] = init_trapz(75 * tweak, 100 * tweak, 0 * tweak, 0 * tweak, right_trapezoid);

	/* The combined x membership functions */
	fz->inp_mem_fns[in_combined_x][in_nl] = init_trapz(0 * tweak, 0 * tweak, -100 * tweak, -75 * tweak, left_trapezoid);
	fz->inp_mem_fns[in_combined_x][in_ns] = init_trapz(-100 * tweak, -75 * tweak, -25 * tweak, -0 * tweak, regular_trapezoid);
	fz->inp_mem_fns[in_combined_x][in_ze] = init_trapz(-25 * tweak, -0 * tweak, 0 * tweak, 25 * tweak, regular_trapezoid);
	fz->inp_mem_fns[in_combined_x][in_ps] = init_trapz(0 * tweak, 25 * tweak, 75 * tweak, 100 * tweak, regular_trapezoid);
	fz->inp_mem_fns[in_combined_x][in_pl] = init_trapz(75 * tweak, 100 * tweak, 0 * tweak, 0 * tweak, right_trapezoid);

	return;
}

void initFuzzySystem(fuzzy_system_rec *fz) {

	//Note: The settings of these parameters will depend upon your fuzzy system design
	fz->no_of_fuzzy_sets = NO_OF_FUZZY_SETS;
	fz->no_of_inputs_per_FAMM = INPUTS_PER_FUZZY_SET;  /* Inputs are handled 2 at a time only */
	fz->no_of_rules_per_FAMM = pow(NO_OF_INP_REGIONS, INPUTS_PER_FUZZY_SET);
	fz->no_of_inp_regions = NO_OF_INP_REGIONS;
	fz->no_of_outputs = NO_OF_OUTPUT_VALUES;

	float tweak = 1.0;

	fz->output_values[out_nvl] = -100.0 * tweak;
	fz->output_values[out_nl] = -75.0 * tweak;
	fz->output_values[out_nm] = -50.0 * tweak;
	fz->output_values[out_ns] = -25.0 * tweak;
	fz->output_values[out_ze] = 0.0 * tweak;
	fz->output_values[out_ps] = 25.0 * tweak;
	fz->output_values[out_pm] = 50.0 * tweak;
	fz->output_values[out_pl] = 75.0 * tweak;
	fz->output_values[out_pvl] = 100.0 * tweak;

	fz->ruleset = (rule **) malloc ((size_t)(fz->no_of_fuzzy_sets * sizeof(rule *)));

	fz->ruleset[matrix_theta_vs_theta_dot] = (rule *) malloc ((size_t)(fz->no_of_rules_per_FAMM * sizeof(rule)));
	fz->ruleset[matrix_x_vs_x_dot] = (rule *) malloc ((size_t)(fz->no_of_rules_per_FAMM * sizeof(rule)));
	fz->ruleset[matrix_x_set_vs_theta_set] = (rule *) malloc ((size_t)(fz->no_of_rules_per_FAMM * sizeof(rule)));

	initFuzzyRules(fz);
	initMembershipFunctions(fz);
	return;
}

//////////////////////////////////////////////////////////////////////////////

trapezoid init_trapz(float x1, float x2, float x3, float x4, trapz_type typ) {

	trapezoid trz;
	trz.a = x1;
	trz.b = x2;
	trz.c = x3;
	trz.d = x4;
	trz.tp = typ;
	switch (trz.tp) {

	case regular_trapezoid:
		trz.l_slope = 1.0 / (trz.b - trz.a);
		trz.r_slope = 1.0 / (trz.c - trz.d);
		break;

	case left_trapezoid:
		trz.r_slope = 1.0 / (trz.c - trz.d);
		trz.l_slope = 0.0;
		break;

	case right_trapezoid:
		trz.l_slope = 1.0 / (trz.b - trz.a);
		trz.r_slope = 0.0;
		break;
	}  /* end switch  */

	return trz;
}  /* end function */

//////////////////////////////////////////////////////////////////////////////

float trapz(float x, trapezoid trz) {
	switch (trz.tp) {

	case left_trapezoid:
		if (x <= trz.c)
			return 1.0;
		if (x >= trz.d)
			return 0.0;
		/* a < x < b */
		return trz.r_slope * (x - trz.d);

	case right_trapezoid:
		if (x <= trz.a)
			return 0.0;
		if (x >= trz.b)
			return 1.0;
		/* a < x < b */
		return trz.l_slope * (x - trz.a);

	case regular_trapezoid:
		if ((x <= trz.a) || (x >= trz.d))
			return 0.0;
		if ((x >= trz.b) && (x <= trz.c))
			return 1.0;
		if ((x >= trz.a) && (x <= trz.b))
			return trz.l_slope * (x - trz.a);
		if ((x >= trz.c) && (x <= trz.d))
			return  trz.r_slope * (x - trz.d);

	}  /* End switch  */

	return 0.0;  /* should not get to this point */
}  /* End function */

//////////////////////////////////////////////////////////////////////////////

float min_of(float values[], int no_of_inps)
{
	int i;
	float val;
	val = values[0];
	for (i = 1; i < no_of_inps; i++)
	{
		if (values[i] < val)
		{
			val = values[i];
		}
	}
	return val;
}

//////////////////////////////////////////////////////////////////////////////

float fuzzy_system(float inputs[], fuzzy_system_rec fz)
{
	float combinedFAMMoutput;

	inputs[in_combined_theta] = fuzzy_math(inputs, fz, matrix_theta_vs_theta_dot);
	inputs[in_combined_x] = fuzzy_math(inputs, fz, matrix_x_vs_x_dot);
	combinedFAMMoutput = fuzzy_math(inputs, fz, matrix_x_set_vs_theta_set);
	
	return combinedFAMMoutput;
}

//////////////////////////////////////////////////////////////////////////////

float fuzzy_math(float inputs[], fuzzy_system_rec fz, int FAMM_to_fuzzify) {
	int i, j;
	short variable_index, fuzzy_set;
	float sum1 = 0.0, sum2 = 0.0, weight;
	float m_values[INPUTS_PER_FUZZY_SET];

	for (i = 0; i < fz.no_of_rules_per_FAMM; i++)
	{
		for (j = 0; j < fz.no_of_inputs_per_FAMM; j++)
		{
			variable_index = (fz.ruleset[FAMM_to_fuzzify])[i].inp_index[j];
			fuzzy_set = (fz.ruleset[FAMM_to_fuzzify])[i].inp_fuzzy_set[j];
			m_values[j] = trapz(inputs[variable_index], fz.inp_mem_fns[variable_index][fuzzy_set]);
		} /* end j */

		weight = min_of(m_values, fz.no_of_inputs_per_FAMM);

		sum1 += weight * fz.output_values[(fz.ruleset[FAMM_to_fuzzify])[i].out_fuzzy_set];
		sum2 += weight;
	} /* end i */

	if (fabs(sum2) < TOO_SMALL) {
		cout << "\r\nFLPRCS Error: Sum2 in fuzzy_system is 0. Press key: " << endl;
		return 0.0;
	}

	return (sum1 / sum2);
}  /* end fuzzy_system */

//////////////////////////////////////////////////////////////////////////////

void free_fuzzy_rules(fuzzy_system_rec *fz) {
	if (fz->allocated)
	{
		free(fz->ruleset[matrix_theta_vs_theta_dot]);
		free(fz->ruleset[matrix_x_vs_x_dot]);
		free(fz->ruleset[matrix_x_set_vs_theta_set]);

		free(fz->ruleset);
	}

	fz->allocated = false;
	return;
}

